- выполнить скрипт /resources/sql/insert_currencies.sql

Для сборки своего контейнера, в директории с Dockerfile необходимо выполнить:

docker build . -t <имя_желаемого_образа>

Запуск контейнера:
docker run sc-order-ws

Запуск контейнера c пробросом конкретных портов:
docker run --name=sc-order-ws -p 8080:8080 sc-order-ws


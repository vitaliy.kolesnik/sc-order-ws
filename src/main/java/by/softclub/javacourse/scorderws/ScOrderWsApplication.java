package by.softclub.javacourse.scorderws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScOrderWsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScOrderWsApplication.class, args);
    }

}

package by.softclub.javacourse.scorderws.controller;

import by.softclub.javacourse.scorderws.dto.OrderDto;
import by.softclub.javacourse.scorderws.service.OrderService;
import by.softclub.javacourse.scorderws.service.impl.KafkaSendService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    private final OrderService orderService;
    private final KafkaSendService sendService;

    public OrderController(OrderService orderService, KafkaSendService sendService) {
        this.orderService = orderService;
        this.sendService = sendService;
    }

    @GetMapping
    public List<OrderDto> getOrders() {
        return orderService.findAll();
    }

    @PostMapping("/create")
    public void createOrder(@Valid @RequestBody OrderDto dto) {
        // отправка объекта в очередь
        sendService.send(dto);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteOrder(@PathVariable int id) {
        orderService.deleteById(id);
    }
}

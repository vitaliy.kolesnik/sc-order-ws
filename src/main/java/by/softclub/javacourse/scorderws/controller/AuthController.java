package by.softclub.javacourse.scorderws.controller;

import by.softclub.javacourse.scorderws.domain.User;
import by.softclub.javacourse.scorderws.dto.LoginDto;
import by.softclub.javacourse.scorderws.service.AuthenticationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    private final AuthenticationService authenticationService;

    public AuthController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping(value = "/login")
    public String login(@RequestBody LoginDto dto) {
        User user = (User) authenticationService.loadUserByUsername(dto.getLogin());
        return authenticationService.login(user, dto);
    }
}

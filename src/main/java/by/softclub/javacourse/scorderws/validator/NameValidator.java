package by.softclub.javacourse.scorderws.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidator implements ConstraintValidator<NameConstraint, String> {

   private String message;
   private String pattern;

   public void initialize(NameConstraint constraint) {
      this.pattern = constraint.pattern();
      this.message = "ERROR_NAME_MATCH_PATTERN";
   }

   public boolean isValid(String name, ConstraintValidatorContext context) {
      boolean isValid = true;

      if (name != null && !name.matches(pattern)) {
         isValid = false;
         context.buildConstraintViolationWithTemplate(this.message).addConstraintViolation();
      }

      return isValid;
   }
}

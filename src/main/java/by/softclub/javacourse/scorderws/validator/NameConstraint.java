package by.softclub.javacourse.scorderws.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NameValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NameConstraint {
    String pattern() default "^[A-z]+$";
    String message() default "ERROR_NAME_MATCH_PATTERN";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

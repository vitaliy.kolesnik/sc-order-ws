package by.softclub.javacourse.scorderws.domain.enums;

public enum OrderState {
    CREATED,
    IN_PROCESS,
    FINISHED,
    CANCELED,
    ERROR;
}

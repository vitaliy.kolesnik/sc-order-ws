package by.softclub.javacourse.scorderws.repository;


import by.softclub.javacourse.scorderws.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsernameEquals(String username);
}

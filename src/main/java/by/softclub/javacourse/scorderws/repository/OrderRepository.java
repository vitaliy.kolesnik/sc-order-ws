package by.softclub.javacourse.scorderws.repository;


import by.softclub.javacourse.scorderws.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}

package by.softclub.javacourse.scorderws.dto;

import lombok.Data;

@Data
public class ErrorDto {
    private String errorCode;
    private String errorText;
}

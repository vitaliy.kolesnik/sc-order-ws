package by.softclub.javacourse.scorderws.dto;

import by.softclub.javacourse.scorderws.domain.enums.OrderType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class OrderDto implements Serializable {

    private int id;

    private String sourceCurrency;

    private String targetCurrency;

    private BigDecimal amount;

    private BigDecimal fee;
}

package by.softclub.javacourse.scorderws.dto;

import by.softclub.javacourse.scorderws.validator.NameConstraint;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginDto {

    @NameConstraint
    private String login;

    private String password;
}

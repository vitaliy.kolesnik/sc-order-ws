package by.softclub.javacourse.scorderws.mapper;

import by.softclub.javacourse.scorderws.domain.Order;
import by.softclub.javacourse.scorderws.dto.OrderDto;

public interface OrderDtoToOrderMapper extends ObjectMapper<OrderDto, Order> {
}

package by.softclub.javacourse.scorderws.mapper.impl;

import by.softclub.javacourse.scorderws.domain.Order;
import by.softclub.javacourse.scorderws.dto.OrderDto;
import by.softclub.javacourse.scorderws.mapper.OrderToOrderDtoMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderToOrderDtoMapperImpl implements OrderToOrderDtoMapper {

    @Override
    public OrderDto map(Order order) {
        var dto = new OrderDto();
        dto.setAmount(order.getAmount());
        dto.setFee(order.getFee());
        dto.setId(order.getId());
        dto.setSourceCurrency(order.getSourceCurrency().getCode());
        dto.setTargetCurrency(order.getTargetCurrency().getCode());
        return dto;
    }

}

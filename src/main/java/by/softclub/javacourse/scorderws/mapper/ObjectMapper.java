package by.softclub.javacourse.scorderws.mapper;

public interface ObjectMapper<T1, T2> {
    T2 map(T1 source);
}

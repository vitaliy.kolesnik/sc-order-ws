package by.softclub.javacourse.scorderws.mapper.impl;

import by.softclub.javacourse.scorderws.domain.Currency;
import by.softclub.javacourse.scorderws.domain.Order;
import by.softclub.javacourse.scorderws.dto.OrderDto;
import by.softclub.javacourse.scorderws.exception.CurrencyNotFoundException;
import by.softclub.javacourse.scorderws.mapper.OrderDtoToOrderMapper;
import by.softclub.javacourse.scorderws.repository.CurrencyRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderDtoToOrderMapperImpl implements OrderDtoToOrderMapper {

    private final CurrencyRepository currencyRepository;

    public OrderDtoToOrderMapperImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public Order map(OrderDto dto) {

        var order = new Order();
        order.setAmount(dto.getAmount());
        order.setFee(dto.getFee());
        order.setSourceCurrency(getCurrency(dto.getSourceCurrency()));
        order.setTargetCurrency(getCurrency(dto.getTargetCurrency()));

        return order;
    }

    private Currency getCurrency(String iso) {
        Optional<Currency> optCurr = currencyRepository.findByCode(iso);
        if (optCurr.isPresent()) {
            return optCurr.get();
        } else {
            throw new CurrencyNotFoundException();
        }
    }

}

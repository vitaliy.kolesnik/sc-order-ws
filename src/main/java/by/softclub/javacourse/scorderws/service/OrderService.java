package by.softclub.javacourse.scorderws.service;


import by.softclub.javacourse.scorderws.dto.OrderDto;

import java.util.List;

public interface OrderService {

    List<OrderDto> findAll();

    void deleteById(int id);
}

package by.softclub.javacourse.scorderws.service;


import by.softclub.javacourse.scorderws.domain.User;
import by.softclub.javacourse.scorderws.dto.LoginDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthenticationService extends UserDetailsService {

    void replaceUserInCache(String token, User user);

    String login(User user, LoginDto loginDto);

}

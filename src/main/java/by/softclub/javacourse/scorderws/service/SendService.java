package by.softclub.javacourse.scorderws.service;

import by.softclub.javacourse.scorderws.dto.OrderDto;

public interface SendService {
    void send(OrderDto dto);
}

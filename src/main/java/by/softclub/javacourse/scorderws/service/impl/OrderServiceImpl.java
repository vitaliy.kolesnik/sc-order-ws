package by.softclub.javacourse.scorderws.service.impl;

import by.softclub.javacourse.scorderws.dto.OrderDto;
import by.softclub.javacourse.scorderws.mapper.OrderDtoToOrderMapper;
import by.softclub.javacourse.scorderws.mapper.OrderToOrderDtoMapper;
import by.softclub.javacourse.scorderws.repository.OrderRepository;
import by.softclub.javacourse.scorderws.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderToOrderDtoMapper orderMapper;

    public OrderServiceImpl(OrderRepository orderRepository, OrderToOrderDtoMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    @Override
    public List<OrderDto> findAll() {
        log.info(SecurityContextHolder.getContext().getAuthentication().toString());
        return orderRepository.findAll().stream()
                .map(orderMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(int id) {
        orderRepository.deleteById(id);
    }
}

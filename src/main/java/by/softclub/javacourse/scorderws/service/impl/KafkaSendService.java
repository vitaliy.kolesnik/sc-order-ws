package by.softclub.javacourse.scorderws.service.impl;

import by.softclub.javacourse.scorderws.dto.OrderDto;
import by.softclub.javacourse.scorderws.service.SendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSendService implements SendService {

    @Autowired
    private KafkaTemplate<Long, OrderDto> kafkaTemplate;

    @Override
    public void send(OrderDto dto) {
        kafkaTemplate.send("softclub", dto);
    }

}

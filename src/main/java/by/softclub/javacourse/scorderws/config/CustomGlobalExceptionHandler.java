package by.softclub.javacourse.scorderws.config;

import by.softclub.javacourse.scorderws.dto.ErrorDto;
import by.softclub.javacourse.scorderws.exception.CurrencyNotFoundException;
import by.softclub.javacourse.scorderws.exception.OrderAlreadyExistsException;
import by.softclub.javacourse.scorderws.service.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private TranslationService translationService;

    @ExceptionHandler(CurrencyNotFoundException.class)
    protected ResponseEntity<Object> handleFilmNotFoundException(CurrencyNotFoundException ex, WebRequest webRequest) {

        return getObjectResponseEntity(ex, webRequest);

    }

    @ExceptionHandler(OrderAlreadyExistsException.class)
    protected ResponseEntity<Object> handleFilmAlreadyExistException(OrderAlreadyExistsException ex, WebRequest webRequest) {

        return getObjectResponseEntity(ex, webRequest);

    }

    private ResponseEntity<Object> getObjectResponseEntity(RuntimeException ex, WebRequest webRequest) {
        ErrorDto errorDto = new ErrorDto();
        String errorText = translationService.getLocalMessage(ex.getMessage(), webRequest.getLocale());

        errorDto.setErrorCode(ex.getMessage());
        errorDto.setErrorText(errorText);

        return handleExceptionInternal(ex, errorDto,
                new HttpHeaders(), NOT_FOUND, webRequest);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        return new ResponseEntity<>(ex.getBindingResult().getAllErrors(), headers, status);
    }

}

package by.softclub.javacourse.scorderws.exception;

public class CurrencyNotFoundException extends RuntimeException {

    public static String error = "ERROR_CURRENCY_NOT_FOUND";

    public CurrencyNotFoundException() {
        super(error);
    }

    public CurrencyNotFoundException(String userError) {
        super(userError);
    }

}

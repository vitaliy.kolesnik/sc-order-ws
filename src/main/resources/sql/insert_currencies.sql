INSERT INTO public.sc_currencies (id, code, description) VALUES (933, 'BYN', 'Белорусский рубль');
INSERT INTO public.sc_currencies (id, code, description) VALUES (840, 'USD', 'Доллар США');
INSERT INTO public.sc_currencies (id, code, description) VALUES (978, 'EUR', 'Евро');
INSERT INTO public.sc_currencies (id, code, description) VALUES (643, 'RUB', 'Российский рубль');
INSERT INTO public.sc_currencies (id, code, description) VALUES (980, 'UAH', 'Украинская гривна');

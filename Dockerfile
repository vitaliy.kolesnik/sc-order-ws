FROM adoptopenjdk/openjdk11
RUN mkdir -p /mnt/app
COPY /target/sc-order-ws.jar /mnt/app
COPY test.txt /mnt/app/test.txt
RUN chmod -R 777 /mnt/app/sc-order-ws.jar
CMD ["java", \
"-jar", \
"-Xms1g", \
"-Xmx2g", \
 "/mnt/app/sc-order-ws.jar"]
